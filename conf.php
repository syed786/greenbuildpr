<?
$config['cp.TimeZone'] = 'Asia/Hong_Kong';
date_default_timezone_set($config['cp.TimeZone']);

define('CP_HOST', $_SERVER['HTTP_HOST']);
//================================================================//
if (CP_HOST == "greenbuildpr.cubosale.com") {
    define('CP_ENV', 'production');
    define('CP_CORE_PATH', '/home/cmsPilot/v3.0/');

} else if (CP_HOST == "greenbuildpr.usoftdev.com") {
    define('CP_ENV', 'testing');
    define('CP_CORE_PATH', '/home/cmsPilot/v3.0/');

} else if (CP_HOST == "greenbuildpr.localhost") {
    $docRoot = $_SERVER['DOCUMENT_ROOT'];
    $rootFolder = substr($docRoot, 0, stripos($docRoot, '/greenbuildpr/'));

    define('CP_ENV', 'local');
    define('CP_CORE_PATH', $rootFolder . '/cmsPilot/v3.0/');
}

define('CP_PATH', CP_CORE_PATH . 'CP/');
//================================================================//
require_once(CP_PATH . 'common/lib/inc_path.php');

/*** Local Server **/
$config['local'] = array(
     'db' => array(
          'host'     => 'localhost'
         ,'username' => 'root'
         ,'password' => @$_SERVER['dbPassword']
         ,'dbname'   => 'greenbuildpr'
         ,'tomcatServer' => 'localhost:8080'
         ,'jasperFolder' => 'JavaBridgeJasper4.0.2'
     )
    ,'display_errors' => true
);

/*** Development Server **/
$config['development'] = $config['local'];
$config['development']['db']['username'] = 'greenbuildpr';
$config['development']['db']['password'] = '8JvadC2KQLfeGnUP';

/*** Testing Server **/
$config['testing'] = $config['development'];
$config['testing']['db']['dbname'] = 'greenbuildpr';
$config['testing']['db']['username'] = 'greenbuildpr_user';
$config['testing']['db']['password'] = '8JvadC2KQLfeGnUP';
$config['testing']['display_errors'] = false;

/*** Production Server **/
$config['production'] = $config['testing'];
$config['production']['display_errors'] = true;
$config['production']['db']['dbname']   = 'greenbuildpr';
$config['production']['db']['username'] = 'greenbuildpr_use';
$config['production']['db']['password'] = 'RWrYDFEsHGe5YF4bN';

//================================================================//
require_once(CP_PATH . 'common/lib/Registry.php');
$cfgCommon = require_once(CP_PATH . 'common/lib/config.php');
$cfgMast = require_once($cfgCommon['cp.masterPath'] . 'lib/config.php');
$cfgLoc  = require_once($cfgCommon['cp.localPath'] . 'lib/config.php');

$cpCfg = array_merge($config, $cfgCommon, $cfgMast, $cfgLoc);
Zend_Registry::set('cpCfg',$cpCfg);
//================================================================//
